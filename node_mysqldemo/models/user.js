var dtime =require('time-formater') 
var Sequelize =require('sequelize')
module.exports = function(sequelize){
    var User = sequelize.define('user',{
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            allowNull:false,
            autoIncrement: true
        },
        name:{
            type:Sequelize.STRING,
            allowNull:false,
        },
        password:{
            type:Sequelize.STRING,
            allowNull:false,
        },
        age:{
            type:Sequelize.INTEGER
        },
        createDate:{
            type:Sequelize.STRING,
            defaultValue:dtime().format('YYYY-MM-DD HH:mm')
        },
        lastLogin:{
            type:Sequelize.STRING,
            defaultValue:dtime().format('YYYY-MM-DD HH:mm')
        },
        active:{
            type:Sequelize.BOOLEAN,
            defaultValue:true
        }
    },{
        freezeTableName:true,
        timestamps: false,
    });
    return User;
}

//静态方法
const classMethods = {
    //根据id查询
    getUserById: function(id) {
        return this.findById(id);
    },
    //获取所有
    getUsers: function(options) {
        return this.findAll(options);
    },
    //根据id更新数据
    updateUserById: function(values, id) {
        return this.update(values, {
            where: {
                id: id
            }
        });
    },
    //根据id删除数据
    deleteById: function(id) {
        return this.destroy({
            where: {
                id: id
            }
        })
    }
}