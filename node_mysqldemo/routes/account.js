var express = require('express')
var router = express.Router();
var crypto = require('crypto');
var {sequelize} =require('../config/db')
var User = sequelize.import('../models/user')
router.get('/login',function(req,res){
    res.render('login')
})

router.post('/login',function(req,res){
    User.findOne({where:{name:req.body.username}}).then(function(user){
        if(!user){
            req.session.error="用户名或密码错误"
            return res.redirect('/account/login')
        }else{
            var md5= crypto.createHash('md5');
            var password = md5.update(req.body.password).digest('base64');
            if(user.password!==password){
                req.session.error="用户名或密码错误"
                return res.redirect('/account/login')
            }
            req.session.user= user;
            req.session.success= "登陆成功";
            res.redirect('/');
        }  
    }) 
})

router.get('/register',function (req, res){
    res.render('register')
})

router.post('/register',function (req, res,next){
    if(req.body['password']!==req.body['passwordconf']){
        console.log('两次输入的密码不一致！')    
    }
    else {
        User.findOne({where:{name:req.body.username}}).then(function(user){
            if(user){
                req.session.error="用户已存在"
            }else{
                var md5= crypto.createHash('md5');
                var password = md5.update(req.body.password).digest('base64');
                var newUser= {
                    name:req.body.username,
                    password:password,
                }
                console.log('do the work done at there')
                User.create({name:req.body.username, password:password,}).then(function(user){
                    req.session.user= user;
                    req.session.success= "注册成功";
                    res.redirect('/')
                })
            }  
        }) 
    }
    
})

router.get('/logout',function(req,res,next){
    req.session.destroy();
    res.redirect('/account/login')
})


module.exports = router;