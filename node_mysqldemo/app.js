var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var chalk = require('chalk');
var swig = require('swig')

var index = require('./routes/index');
var users = require('./routes/users');
var account = require('./routes/account');

var app = express();
app.use(session({
  secret: 'lee',
  name: 'mysqldemo',
  cookie:{maxAge:80000},
  resave:false,
  saveUninitialized:true
}))


// view engine setup
app.set('views', path.join(__dirname, 'views'));
//注销jade 引擎，改用swig模板引擎
//app.set('view engine', 'jade');
app.set('view engine','html');
app.engine('html',swig.renderFile);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req,res,next){
  var err= req.session.error;
  var success= req.session.success;
  var user= req.session.user;
  var mess= req.session.message;
  delete req.session.success;
  delete req.session.error;
  delete req.session.message;
  if(err){
    res.locals.err= err;
  }
  if(mess){
    res.locals.message=  mess; 
  }
  if(success){
    res.locals.success=success ;
  }
  if(user){
    res.locals.user=user;
  }
  next();
});


app.use('/', index);
app.use('/users', users);
app.use('/account',account);

//添加对数据库连接的注册,并用于同步模型到数据库
require('./models/ref')
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  //res.locals.message = err.message;
  //res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
