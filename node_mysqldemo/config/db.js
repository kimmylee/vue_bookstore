var chalk = require('chalk');
var mysql = require('mysql');
const Sequelize = require('sequelize');
var setting = require('./setting')


// 配置数据库连接
const sequelize= new Sequelize(setting.dbname, setting.dbuser, setting.password,{
    host:setting.dbhost,
    dialect : 'mysql',
    pool:{
        max:5,
        min:0,
        acquire:30000,
        idle:10000
    }
});
//测试数据库链接
sequelize.authenticate().then(function() {
    console.log(chalk.green("数据库连接成功"));
}).catch(function(err) {
    //数据库连接失败时打印输出
    console.error(err);
    throw err;
});

exports.sequelize = sequelize;
exports.Sequelize = Sequelize;