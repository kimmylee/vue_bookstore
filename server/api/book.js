var express = require('express');
var db= require('../db');

var Book = require('../models/book');

exports.bookList= function(req,res){
    Book.find(function(err,books){
        console.log('books api');
        if(err){
            res.send({
                code:-200,
                message:err.toString()
            })
        }
        else{
            res.send({
                code:200,
                books
            });
        } 
    });
}

