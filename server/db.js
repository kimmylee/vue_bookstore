var chalk = require('chalk');
var mongoose= require('mongoose');

var dburl="localhost:27017";
var dbname="elm";

mongoose.connect("mongodb://"+dburl+"/"+dbname);

var db= mongoose.connection;
db.once('open' ,() => {
	console.log(
    chalk.green('连接数据库成功')
  );
})

db.on('error', function(error) {
    console.error(
      chalk.red('Error in MongoDb connection: ' + error)
    );
    mongoose.disconnect();
});

db.on('close', function() {
    console.log(
      chalk.red('数据库断开，重新连接数据库')
    );
    mongoose.connect("mongodb://"+dburl+"/"+dbname, {server:{auto_reconnect:true}});
});

module.exports= {
    "dbCon":db,
    "mongoose":mongoose
};
