var express = require('express');
var router = express.Router();
var Shop = require('../models/shop');
var createToken = require('../middleware/createToken')
var upload = require('../middleware/fileupload')

/* GET users listing. */
router.post('/shop/addshop', function(req, res, next) {
    var newShop= new Shop({
        name:req.body.name,
        address:req.body.address,
        latitude:req.body.latitude,
        longitude:req.body.longitude,
        description:req.body.description,
        phone:req.body.phone,
        promotion_info:req.body.promotion_info,
        float_delivery_fee:req.body.float_delivery_fee,
        float_minimum_order_amount:req.body.float_minimum_order_amount,
        is_premium:req.body.is_premium,
        delivery_mode:req.body.delivery_mode,
        bao:req.body.bao,
        zhun:req.body.zhun,
        piao:req.body.piao,
        startTime:req.body.startTime,
        endTime:req.body.endTime,
        image_path:req.body.image_path,
        business_license_image:req.body.business_license_image,
        catering_service_license_image:req.body.catering_service_license_image,
    })
    Shop.findOne({name:newShop.name,address:newShop.address},function(err,dbShop){
        if(dbShop){
            return res.json({code:-200,message:"店铺已存在"})
        }
        if(err){
            return res.json({code:-200,message:err.message})
        }
        newShop.save(function(err){
            if(err){
                console.log(err.message)
                return res.json({code:-200,message:err.message})
            }
            res.send({
              code:200
            })
          })
    })
});
router.get('/shop/list',function(req,res,next){
    Shop.find({},function(err,shoplist){
      if(shoplist){
        res.send({
          code:200,
          shoplist
        })
      }
    })
  });
router.post('/shop/addImg',upload.single('file'),function(req,res,next){
    console.log(req.file)
    if(req.file){
        res.send({
            code:200,
            filepath:req.file.filename
        })
    }
})
module.exports = router;