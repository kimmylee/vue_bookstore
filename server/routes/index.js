// var express = require('express');
// var router = express.Router();

// /* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

// module.exports = router;



module.exports= function(app){
  app.use('/admin',require('./admin'));
  app.use('/api',require('./user'));
  app.use('/api',require('./shop'));
}
