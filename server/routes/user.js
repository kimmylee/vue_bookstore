var express = require('express');
var router = express.Router();
var User = require('../models/user');
var sha1 =require('sha1')
var createToken = require('../middleware/createToken')

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/user/list',function(req,res,next){
  const {limit = 20, offset = 0} = req.query;
  try{
    User.find({},'-password',{skip:Number(offset),limit:Number(limit),sort:{"-create_time":1}},function(err,userlist){
      console.log(userlist)
      if(userlist){
        res.send({
          code:200,
          userlist
        })
      }
    })
  }catch(err){
    res.send({
      code:-200,
      type:'ERROR_GET_USER_LIST',
      message:"获取用户列表失败"
    })
  }
});
router.get('/user/count',function(req,res,next){
  try{
    const count= User.count(function(err,count){
      if(count){
        res.send({
          code:200,
          count
        })
      }
    })
    
  }catch(err){
    res.send({
      code:-200,
      type:'ERROR_GET_USER_LIST',
      message:"获取用户列表失败"
    })
  }
});
router.post('/register',function(req,res,next){
  var name= req.body.user_name;
  var password= sha1(req.body.password);
  var newUser= new User({
    user_name:name,
    password:password,
    admin:'管理员'
  });
  
  User.findOne({name:name},function(err,dbUser){
    if(dbUser){
      return res.json({code:-200,message:"用户已存在"})
    }
    if(err){
      return res.json({code : -200,message:err.message})
    }
    newUser.save(function(err){
      res.send({
        code:200,
        token:createToken(name)
      })
    })
  })
})

router.post('/login',function(req,res,next){
  var name= req.body.user_name;
  var password = sha1(req.body.password);
  User.findOne({name:name},function(err,dbUser){
    if(!dbUser){
      var name= req.body.user_name;
      var password= sha1(req.body.password);
      var newUser= new User({
        user_name:name,
        password:password,
        admin:'管理员'
      });
      newUser.save(function(err){
        res.send({
          code:200,
          token:createToken(name),
          userInfo:newUser
        })
      })
    }else if(password == dbUser.password){     
      res.json({
        code:-200,
        message:"用户名或密码错误"
      })
    }
    else{
      User.findOneAndUpdate({_id:dbUser._id},{$set:{last_login:Date.now}})
      res.json({code:200,token:createToken(dbUser.name),userInfo:dbUser})
    }
  }).catch(function(err){
      next(err)
      return res.json({
          code:-200,
          message:err.toString()
      })
  })
})
module.exports = router;
