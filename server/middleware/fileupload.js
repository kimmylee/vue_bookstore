var multer = require('multer')
var md5= require('md5')


var path = process.cwd()+"/uploads"

var storage = multer.diskStorage({
    destination: path,
    filename:function(req,file,cb){
        var fileFormat = (file.originalname).split(".");
        console.log('filepaht',file)
        cb(null, fileFormat[0] + '-' + md5(file) + "." + fileFormat[fileFormat.length - 1]);
    }
})

var upload = multer({
    storage:storage,
})
module.exports= upload;