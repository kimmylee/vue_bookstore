var mongoose = require('../db').mongoose

var schema = new mongoose.Schema({
  user_name: String,
	password: String,
	create_time: {type:Date,default: Date.now},
	admin: {type: String, default: '管理员'},
	status: Number,  //1:普通管理、 2:超级管理员
	avatar: {type: String, default: 'default.jpg'},
	city: String,
	last_login: {type:Date,default: Date.now}
})

var User = mongoose.model('Admin', schema)
module.exports = User
