var mongoose = require('../db').mongoose

var schema = new mongoose.Schema({
  name: {type: String, required: true},
  address:  {type: String, required: true},
  latitude:{type:String},
  longitude:{type:String},
  description:String,
  phone: String,
  promotion_info:String,
  float_delivery_fee:{type:Number},
  float_minimum_order_amount:Number,
  is_premium:Boolean,
  delivery_mode:Boolean,
  bao:Boolean,
  zhun:Boolean,
  piao:Boolean,
  startTime:{type:String,required:true},
  endTime:{type:String,required:true},
  image_path:String,
  business_license_image:String,
  catering_service_license_image:String,
  addDate: {type: Date, default: Date.now}
})

var Shop = mongoose.model('Shop', schema)

module.exports = Shop
