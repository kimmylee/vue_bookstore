var mongoose = require('../db').mongoose

var schema = new mongoose.Schema({
  title: {type: String, required: true},
  picture: String,
  price: {type: Number, required: true},
  author: String,
  addDate: {type: Date, default: Date.now}
})

var Book = mongoose.model('Book', schema)

module.exports = Book
