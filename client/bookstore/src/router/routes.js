
import Login from '@/pages/backEnd/Login'
import BackendLayout from '@/pages/backend/backendLayout'
import Home from '@/pages/backend/home'
import UserList from '@/pages/backend/userlist'
import AddShop from '@/pages/backend/addshop'
import ShopList from '@/pages/backend/shoplist'
// import Front from '@/components/fronted/Front'

export default [
  // {
  //   path: '/register',
  //   component: Register,   
  //   meta: {auth: false, menu: ['注册']},
  //   hidden: true
  // },
  {
    path: '/login',
    component: Login,
    meta: {auth: false, menu: ['登录']},
    hidden: true
  }, {
    path: '/admin',
    component: BackendLayout, // 后台首页
    children: [
      {path: '', redirect: 'home', meta: {auth: true, menu: ['首页']}},
      {path: 'home', component: Home, meta: {auth: true, menu: ['首页']}},
      {path: 'userlist', component: UserList, meta: {auth: true, menu: ['数据管理', '用户列表']}},
      {path: 'addshop', component: AddShop, meta: {auth: true, menu: ['添加数据', '添加商铺']}},
      {path: 'shoplist', component: ShopList, meta: {auth: true, menu: ['数据管理', '商铺列表']}}
    ]
  }
]
