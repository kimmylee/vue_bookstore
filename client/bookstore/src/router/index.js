import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import routes from './routes'
Vue.use(Router)

const scrollBehavior = (to, from, savedPosition) => {
  if (savedPosition) {
    return savedPosition
  } else {
    return {x: 0, y: 0}
  }
}

const router = new Router({
  mode: 'history',
  scrollBehavior,
  routes
})

// 设置路由钩子
router.beforeEach(({meta, path}, from, next) => {
  store.dispatch('showProgress', 0)
  let {auth = true} = meta
  let isLogin = Boolean(store.state.token)

  /*
    访问不需要权限的设置 meta:false
    注册也要设置成meta:false
  */
  if (auth && !isLogin && path !== '/login') {
    return next({path: '/login'})
  }
  if (isLogin && (path === '/login' || path === '/register')) {
    return next({path: '/admin'})
  }

  next()
})
export default router
