import api from '../api'
import router from '../router'
import MsgAlert from './MsgAlert'

export default {
  UserRegister ({commit}, data) {
    api.register(data)
      .then(({data}) => {
        if (data.code === 200) {
          commit('USER_REG', data.token, data.userInfo)
          router.replace({path: '/admin'})
        } else {
          MsgAlert(data.message)
        }
      }).catch((error) => {
        MsgAlert(error.toString())
      })
  },

  UserLogin ({commit}, data) {
    api.login(data)
      .then(({data}) => {
        console.log(data)
        if (data.code === 200) {
          commit('USER_SIGNIN', data.token, data.userInfo)
          router.replace({path: '/admin'})
        } else {
          MsgAlert(data.message)
        }
      }).catch(error => {
        MsgAlert(error.toString())
      })
  },
  UserLogout ({commit}) {
    commit('USER_SIGNOUT')
    router.push({path: '/login'})
  },
  showProgress ({commit}, number) {
    commit('SHOW_PROGRESS', number)
  },
  changeHeadLine ({commit}, headline) {
    commit('HEAD_LINE', headline)
  },
  GetUserInfo ({commit}) {
    let userInfo = localStorage.getItem('userinfo')
    if (userInfo) {
      console.log(userInfo)
      return userInfo
    } else {
      commit('USER_SIGNOUT')
      router.push({path: '/login'})
    }
  }
}
