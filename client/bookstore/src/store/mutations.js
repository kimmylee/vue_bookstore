import {USER_SIGNIN, USER_SIGNOUT, USER_REG, SHOW_PROGRESS, HEAD_LINE} from './types'
export default {
  [USER_REG] (state, token, userInfo) {
    localStorage.setItem('jwt', token)
    localStorage.setItem('userinfo', userInfo)
    state.token = token
  },
  [USER_SIGNIN] (state, token, userInfo) {
    localStorage.setItem('jwt', token)
    localStorage.setItem('userinfo', userInfo)
    state.token = token
  },
  [USER_SIGNOUT] (state) {
    localStorage.removeItem('jwt')
    localStorage.removeItem('userinfo')
    state.token = null
  },
  [SHOW_PROGRESS] (state, number) {
    state.progress = number
  },
  [HEAD_LINE] (state, headline) {
    state.headline = headline
  }
}
