export default {
  token: isLoggedIn() || null,
  progress: 0,
  headline: '',
  userInfo: getUserInfo() || null
}

function isLoggedIn () {
  let token = localStorage.getItem('jwt')
  if (token) {
    const payload = JSON.parse(window.atob(token.split('.')[1]))
    // 判断token是否过期，如果过期了访问时会路由到login页面
    if (payload.exp > Date.now() / 1000) {
      return token
    }
  } else {
    return false
  }
}
function getUserInfo () {
  let userInfo = localStorage.getItem('userinfo')
  if (userInfo) {
    return userInfo
  } else {
    return null
  }
}
