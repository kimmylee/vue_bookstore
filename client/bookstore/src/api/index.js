// 各种api
// 负责用model 和服务端交互

import axios from 'axios'
import store from '../store'
// axios.defaults.headers.common['Authorization'] = 'dailu';
axios.defaults.headers.post['Content-Type'] = 'application/json'

const instance = axios.create()
const front_instance = axios.create()
instance.defaults.headers.post['Content-Type'] = 'application/json'
if (localStorage.getItem('jwt')) {
  /* localStorage.getItem('jwt')是带引号的字符串
    Bearer token(通过Authorization头部字段发送到服务端便于验证)的格式：Bearer XXXXXXXXXX
  */
  instance.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('jwt').replace(/(^\")|(\"$)/g, '')
}
// axios拦截请求
axios.interceptors.request.use = instance.interceptors.request.use = front_instance.interceptors.request.use
front_instance.interceptors.request.use(config => {
  store.dispatch('showProgress', 20)
  return config
}, err => {
  // store.dispatch('showProgress',100)
  return Promise.reject(err)
})

front_instance.interceptors.response.use(response => {
  store.dispatch('showProgress', 100)
  return response
}, err => {
  store.dispatch('showProgress', 100)
  return Promise.reject(err)
})

export default {
  register (data) {
    return axios.post('/api/register', data)
  },
  login (data) {
    return axios.post('/api/login', data)
  },
  getusercount(data){
    return axios.get('/api/user/count',data)
  },
  userlist (data) {
    return axios.get('/api/user/list', data)
  },
  foodCategory (data) {
    return axios.get('/api/food/foodcategory', data)
  },
  addShop (data) {
    return axios.post('/api/shop/addshop', data)
  },
  shoplist (data) {
    return axios.get('/api/shop/list', data)
  },
  addShopImg (data) {
    console.log('start upload file')
    axios.defaults.headers.post['Content-Type'] = 'multipart/form-data'
    return axios.post('/api/shop/addImg', data)
  }

}
