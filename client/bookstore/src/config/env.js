let routerMode = 'history'
let baseImgPath = 'http://localhost:3009/uploads/'
let baseurl = 'http://localhost:3009/api/'
let defaultImgPath = 'http://localhost:8080/img/default.jpg'
export {
  routerMode,
  baseImgPath,
  baseurl,
  defaultImgPath
}
