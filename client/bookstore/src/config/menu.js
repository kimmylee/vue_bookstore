let menuItems = [{
  path: '/admin',
  index: 'home',
  menuName: '首页',
  hasChild: false,
  icon: 'el-icon-menu'
}, {
  path: '#',
  index: '2',
  menuName: '数据管理',
  hasChild: true,
  icon: 'el-icon-document',
  children: [
    {
      path: 'userList',
      menuName: '用户列表',
      haschild: false
    }, {
      path: 'shopList',
      menuName: '商家列表',
      haschild: false
    }, {
      path: 'foodList',
      menuName: '食品列表',
      haschild: false
    }

  ]
}, {
  path: '#',
  index: '3',
  menuName: '添加数据',
  hasChild: true,
  icon: 'el-icon-plus',
  children: [
    {
      path: 'addShop',
      menuName: '添加商铺',
      haschild: false
    }, {
      path: 'addGoods',
      menuName: '添加商品',
      haschild: false
    }, {
      path: 'foodList',
      menuName: '食品列表',
      haschild: false
    }

  ]
}]

export {
  menuItems
}
